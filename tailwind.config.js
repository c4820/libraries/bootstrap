/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./projects/apps/bootstrap-ui/src/**/*.{html,js}"],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    visibility: false
  }
}
