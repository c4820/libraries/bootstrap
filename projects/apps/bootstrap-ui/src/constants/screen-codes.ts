export class ScreenCodes {

  //#region Properties

  public static readonly HOME_PAGE = 'DASHBOARD';

  public static readonly DIALOGS_DEMO_PAGE = 'DIALOG_DEMO';

  //#endregion

}
