import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRouteModule} from './app-routing.module';
import {AppConfigService} from '../services/implementations/app-config.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../factories/ngx-translate.factory';
import {HttpClient} from '@angular/common/http';
import {MainScreenCodeResolve} from '../resolves/screen-codes/main-screen-code.resolve';
import {SMART_NAVIGATOR_SCREEN_CODE_RESOLVER, SmartNavigatorModule} from '@ui-tool/core';
import {MainLayoutModule} from './cores/main-layout/main-layout.module';

//#region Module declaration

@NgModule({
  declarations: [],
  imports: [
    AppRouteModule,

    SmartNavigatorModule.forRoot(),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MainLayoutModule
  ],
  providers: [
    AppConfigService,
    {
      provide: SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
      useClass: MainScreenCodeResolve,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})


export class AppModule {
}

//#endregion
