import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {ServiceModule} from '../services/service.module';
import {MainLayoutComponent} from './cores/main-layout/main-layout.component';

//#endregion

//#region Properties

// Application routes configuration.
export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MainLayoutComponent,
        children: [
          {
            path: 'dialog-demo',
            loadChildren: () => import('./pages/dialog-demo/dialog-demo.module')
              .then(m => m.DialogDemoModule)
          },
          {
            path: '',
            pathMatch: 'full',
            loadChildren: () => import('./pages/home-page/home-page.module')
              .then(m => m.HomePageModule)
          },
        ]
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/home-page'
      },
    ]
  }
];

//#endregion

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule, // required animations module
    HttpClientModule,
    ServiceModule.forRoot(),
    RouterModule.forRoot(routes, {enableTracing: false, relativeLinkResolution: 'legacy'})
  ],
  exports: [
    RouterModule
  ],
  bootstrap: [AppComponent]
})

export class AppRouteModule {
}
