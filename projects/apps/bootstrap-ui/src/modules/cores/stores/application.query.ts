import {Query} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {IApplicationState} from './application.state';
import {ApplicationStore} from './application.store';

@Injectable({
  providedIn: 'root'
})
export class ApplicationQuery extends Query<IApplicationState> {

  //#region Properties

  public readonly version$ = this.select('version');

  //#endregion


  //#region Constructor

  public constructor(protected readonly _store: ApplicationStore) {
    super(_store);
  }

  //#endregion
}
