import {Store, StoreConfig} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {IApplicationState} from "./application.state";

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'application'})
export class ApplicationStore extends Store<IApplicationState> {

  //#region Constructor

  public constructor() {
    super({
      version: '',
      name: ''
    });
  }

  //#endregion

  //#region Methods

  public setVersion(version: string): void {
    this.update({
      version
    });
  }

  //#endregion

}
