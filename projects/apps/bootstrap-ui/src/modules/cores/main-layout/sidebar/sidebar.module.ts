import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SmartNavigatorModule} from '@ui-tool/core';
import {SidebarComponent} from './sidebar.component';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    RouterModule,
    SmartNavigatorModule
  ],
  exports: [
    SidebarComponent
  ]
})
export class SidebarModule {

}
