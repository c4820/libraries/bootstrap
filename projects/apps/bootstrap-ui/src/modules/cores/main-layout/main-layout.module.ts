import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NavigationBarModule} from './navigation-bar/navigation-bar.module';
import {SidebarModule} from './sidebar/sidebar.module';
import {MainLayoutComponent} from './main-layout.component';

@NgModule({
  imports: [NavigationBarModule, SidebarModule, RouterModule],
  declarations: [
    MainLayoutComponent
  ]
})
export class MainLayoutModule {

}
