import {NgModule} from '@angular/core';
import {NavigationBarComponent} from './navigation-bar.component';

@NgModule({
  exports: [
    NavigationBarComponent
  ],
  declarations: [
    NavigationBarComponent
  ]
})
export class NavigationBarModule {
}
