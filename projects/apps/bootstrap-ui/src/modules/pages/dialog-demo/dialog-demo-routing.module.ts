import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

//#region Methods

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./basic-usage/dialog-demo-basic-usage-page.module')
      .then(m => m.DialogDemoBasicUsagePageModule)
  }
];

//#endregion

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class DialogDemoRoutingModule {

}
