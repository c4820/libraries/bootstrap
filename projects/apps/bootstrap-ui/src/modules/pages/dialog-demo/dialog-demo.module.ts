import {NgModule} from '@angular/core';
import {DialogDemoRoutingModule} from './dialog-demo-routing.module';

@NgModule({
  imports: [
    DialogDemoRoutingModule
  ]
})
export class DialogDemoModule {

}
