import {NgModule} from '@angular/core';
import {DialogDemoBasicUsageRoutingModule} from './dialog-demo-basic-usage-routing.module';
import {DialogDemoBasicUsagePageComponent} from './dialog-demo-basic-usage-page.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  BS_DIALOG_DEFAULT_CONFIRMATION_ICON_PROVIDER,
  BS_DIALOG_DEFAULT_ERROR_ICON_PROVIDER, BS_DIALOG_DEFAULT_INFO_ICON_PROVIDER,
  BS_DIALOG_DEFAULT_WARNING_ICON_PROVIDER,
  BsDialogModule
} from '@ui-tool/bootstrap';
import {HtmlContent, WindowAccessorModule} from '@ui-tool/core';

@NgModule({
  declarations: [
    DialogDemoBasicUsagePageComponent
  ],
  imports: [
    DialogDemoBasicUsageRoutingModule,
    TranslateModule,
    WindowAccessorModule,
    BsDialogModule.forRoot()
  ],
  providers: [
    {
      provide: BS_DIALOG_DEFAULT_WARNING_ICON_PROVIDER,
      useValue: new HtmlContent(`<div class="flex justify-content-center"><img src="/assets/images/sniper-warning.png"></div>`)
    },
    {
      provide: BS_DIALOG_DEFAULT_CONFIRMATION_ICON_PROVIDER,
      useValue: new HtmlContent(`<div class="flex justify-content-center"><img src="/assets/images/sniper-warning.png"></div>`)
    },
    {
      provide: BS_DIALOG_DEFAULT_ERROR_ICON_PROVIDER,
      useValue: new HtmlContent(`<div class="flex justify-content-center""><img src="/assets/images/sniper-warning.png"></div>`)
    },
    {
      provide: BS_DIALOG_DEFAULT_INFO_ICON_PROVIDER,
      useValue: new HtmlContent(`<div class="flex justify-content-center""><img src="/assets/images/sniper-warning.png"></div>`)
    }
  ]
})
export class DialogDemoBasicUsagePageModule {
}
