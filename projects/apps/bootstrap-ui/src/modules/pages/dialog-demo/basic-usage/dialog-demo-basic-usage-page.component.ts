import {ChangeDetectionStrategy, Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {
  BasicDialogButton,
  ConfirmationDialogSettings,
  DIALOG_SERVICE, DialogResult, DialogResultConstant,
  ErrorDialogSettings,
  HtmlContent,
  IDialogService,
  InfoDialogSettings,
  WarningDialogSettings
} from '@ui-tool/core';
import {catchError, Subscription, throwError} from 'rxjs';

@Component({
  selector: 'dialog-demo-basic-usage',
  templateUrl: 'dialog-demo-basic-usage-page.component.html',
  styleUrls: ['dialog-demo-basic-usage-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogDemoBasicUsagePageComponent implements OnInit, OnDestroy {

  //#region Properties

  protected readonly subscription: Subscription;

  //#endregion

  //#region Accessors

  protected readonly _dialogService: IDialogService;

  //#endregion

  //#region Constructor

  public constructor(injector: Injector) {

    // Subscription initialization.
    this.subscription = new Subscription();

    this._dialogService = injector.get(DIALOG_SERVICE);
  }

  //#endregion

  //#region Life cycle

  public ngOnInit(): void {
  }


  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  //#endregion

  //#region Methods

  public displayConfirmationDialog(defaultIcon?: boolean): void {
    const messageHtmlContent = new HtmlContent(`<h3 class="text-center text-primary font-weight-bold">This is confirmation message</h3>`);
    const confirmationDialogSettings = new ConfirmationDialogSettings(messageHtmlContent,
      'This is dialog title');

    if (!defaultIcon) {
      confirmationDialogSettings.icon = new HtmlContent(`<div class="flex flex-column align-items-center"><img class="dialog-icon" src="/assets/images/confirmation.png"></div>`);
    }

    confirmationDialogSettings.dialogClasses = ['confirmation'];
    confirmationDialogSettings.buttons = [
      new BasicDialogButton('OK', () => new DialogResult(DialogResultConstant.resolve, 'Hello world'), ['btn btn-outline-primary']),
      new BasicDialogButton('Cancel', () => new DialogResult(DialogResultConstant.reject, 'Rejected'), ['btn btn-outline-danger'])
    ];
    const displayDialogSubscription = this._dialogService
      .displayDialogAsync(confirmationDialogSettings)
      .subscribe(value => {
        console.log('Resolve with ' + value);
      });

    this.subscription.add(displayDialogSubscription);
  }

  public displayErrorDialog(defaultIcon?: boolean): void {
    const messageHtmlContent = new HtmlContent(`<h3 class="text-center text-danger font-weight-bold">This is danger message</h3>`);
    const confirmationDialogSettings = new ErrorDialogSettings(messageHtmlContent,
      'This is dialog title');

    if (!defaultIcon) {
      confirmationDialogSettings.icon = new HtmlContent(`<div class="flex justify-content-center"><img class="img-fluid" src="/assets/images/danger.png"></div>`);
    }

    const displayDialogSubscription = this._dialogService
      .displayDialogAsync(confirmationDialogSettings)
      .pipe(
        catchError(exception => {
          console.log('Rejected with exception: ' + exception);
          return throwError(() => exception);
        })
      )
      .subscribe();

    this.subscription.add(displayDialogSubscription);
  }

  public displayInfoDialog(defaultIcon?: boolean): void {
    const messageHtmlContent = new HtmlContent(`<h3 class="text-center text-info font-weight-bold">This is info message</h3>`);
    const confirmationDialogSettings = new InfoDialogSettings(messageHtmlContent,
      'This is dialog title');

    if (!defaultIcon) {
      confirmationDialogSettings.icon = new HtmlContent(`<div class="flex justify-content-center"><img class="img-fluid" src="/assets/images/info.png"></div>`);
    }

    const displayDialogSubscription = this._dialogService
      .displayDialogAsync(confirmationDialogSettings)
      .subscribe();

    this.subscription.add(displayDialogSubscription);
  }

  public displayWarningDialog(defaultIcon?: boolean): void {

    const messageHtmlContent = new HtmlContent(`<h3 class="text-center text-warning font-weight-bold">This is warning message</h3>`);
    const warningDialogSettings = new WarningDialogSettings(messageHtmlContent,
      'This is dialog title');

    if (!defaultIcon) {
      // tslint:disable-next-line:max-line-length
      warningDialogSettings.icon = new HtmlContent(`<div class="flex justify-content-center"><img src="/assets/images/warning.png"></div>`);
    }

    const displayWarningDialogSubscription = this._dialogService
      .displayDialogAsync(warningDialogSettings)
      .subscribe();

    this.subscription.add(displayWarningDialogSubscription);
  }

  //#endregion
}
