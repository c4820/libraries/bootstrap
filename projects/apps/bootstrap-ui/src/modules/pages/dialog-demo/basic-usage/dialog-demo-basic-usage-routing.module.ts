import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DialogDemoBasicUsagePageComponent} from './dialog-demo-basic-usage-page.component';

const routes: Routes = [
  {
    path: '',
    component: DialogDemoBasicUsagePageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class DialogDemoBasicUsageRoutingModule {

}
