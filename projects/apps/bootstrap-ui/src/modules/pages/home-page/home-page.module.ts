import {HomePageComponent} from './home-page.component';
import {NgModule} from '@angular/core';
import {DashboardRouteModule} from './home-page-routing.module';

//#region Routes declaration


//#endregion

//#region Module declaration

@NgModule({
  imports: [
    DashboardRouteModule
  ]
})

export class HomePageModule {
}

//#endregion
