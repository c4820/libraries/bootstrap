import {DefaultScreenCodeResolver, IScreenCodeResolver} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {ScreenCodes} from '../../constants/screen-codes';

@Injectable()
export class MainScreenCodeResolve extends DefaultScreenCodeResolver {

  //#region Constructor

  constructor() {
    const codeToUrl: { [p: string]: string } = {};
    codeToUrl[ScreenCodes.HOME_PAGE] = '/home-page';
    codeToUrl[ScreenCodes.DIALOGS_DEMO_PAGE] = '/dialog-demo';

    super(codeToUrl);
  }

//#endregion

  //#region Methods

  //#endregion

}
