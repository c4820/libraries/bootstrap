import {HtmlContent} from '@ui-tool/core';
import {TemplateRef} from '@angular/core';

// Icon type
export type BS_DIALOG_ICON_TYPE = HtmlContent | TemplateRef<any> | undefined;
export type BS_DIALOG_CONTENT_TYPE = string | HtmlContent | TemplateRef<any>;
export type BS_DIALOG_TITLE_TYPE = string | HtmlContent | TemplateRef<any>;
