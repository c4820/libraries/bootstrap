import {HtmlContent} from '@ui-tool/core';
import {TemplateRef} from '@angular/core';

// Dialog button content type.
export type DIALOG_BUTTON_CONTENT_TYPE = string | HtmlContent | TemplateRef<any>;
