import {NgModule} from '@angular/core';
import {ToContentTemplatePipe} from './to-content-template.pipe';

@NgModule({
  exports: [
    ToContentTemplatePipe
  ],
  declarations: [
    ToContentTemplatePipe
  ]
})
export class ToContentTemplateModule {

}
