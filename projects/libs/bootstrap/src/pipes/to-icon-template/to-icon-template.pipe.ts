import {Pipe, PipeTransform, TemplateRef} from '@angular/core';

@Pipe({
  name: 'toIconTemplate'
})
export class ToIconTemplatePipe implements PipeTransform {


  //#region Methods

  public transform(value: any): TemplateRef<any> | null {

    if (value instanceof TemplateRef) {
      return value as TemplateRef<any>;
    }

    return null;
  }

  //#endregion

}
