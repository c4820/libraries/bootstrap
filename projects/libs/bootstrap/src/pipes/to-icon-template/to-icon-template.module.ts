import {NgModule} from '@angular/core';
import {ToIconTemplatePipe} from './to-icon-template.pipe';

@NgModule({
  exports: [
    ToIconTemplatePipe
  ],
  declarations: [ToIconTemplatePipe]
})
export class ToIconTemplateModule {
}
