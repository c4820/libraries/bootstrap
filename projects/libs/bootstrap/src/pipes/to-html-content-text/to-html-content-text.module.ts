import {NgModule} from '@angular/core';
import {ToHtmlContentTextPipe} from './to-html-content-text.pipe';

@NgModule({
  exports: [
    ToHtmlContentTextPipe
  ],
  declarations: [
    ToHtmlContentTextPipe
  ]
})
export class ToHtmlContentTextModule {

}
