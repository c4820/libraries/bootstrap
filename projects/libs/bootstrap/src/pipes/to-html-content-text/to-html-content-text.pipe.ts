import {Pipe, PipeTransform} from '@angular/core';
import {HtmlContent} from '@ui-tool/core';

@Pipe({
  name: 'toHtmlContentText'
})
export class ToHtmlContentTextPipe implements PipeTransform {

  //#region Methods

  public transform(value: any): string {
    if (value instanceof HtmlContent) {
      return (value as HtmlContent).content || '';
    }

    return '';
  }

  //#endregion
}
