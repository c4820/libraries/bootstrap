import {NgModule} from '@angular/core';
import {HasHtmlContentPipe} from './has-html-content.pipe';

@NgModule({
  exports: [
    HasHtmlContentPipe
  ],
  declarations: [
    HasHtmlContentPipe
  ]
})
export class HasHtmlContentModule {

}
