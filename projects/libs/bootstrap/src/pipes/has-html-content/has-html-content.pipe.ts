import {Pipe, PipeTransform} from '@angular/core';
import {HtmlContent} from '@ui-tool/core';

@Pipe({
  name: 'hasHtmlContent'
})
export class HasHtmlContentPipe implements PipeTransform {

  //#region Methods

  public transform(value: any): boolean {
    return value instanceof HtmlContent;
  }

  //#endregion
}
