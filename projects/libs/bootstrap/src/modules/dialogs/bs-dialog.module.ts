import {Injector, ModuleWithProviders, NgModule} from '@angular/core';
import {BsDialogComponent} from './bs-dialog.component';
import {CommonModule} from '@angular/common';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {BsDialogService} from '../../services/implementations/bs-dialog.service';
import {DIALOG_SERVICE} from '@ui-tool/core';
import {ToContentTemplateModule} from '../../pipes/to-content-template/to-content-template.module';
import {ToIconTemplateModule} from '../../pipes/to-icon-template/to-icon-template.module';
import {HasHtmlContentModule} from '../../pipes/has-html-content/has-html-content.module';
import {ToHtmlContentTextModule} from '../../pipes/to-html-content-text/to-html-content-text.module';

const defaultBsDialogServiceFactory = (injector: Injector) => {
  return new BsDialogService(injector);
};

@NgModule({
  declarations: [
    BsDialogComponent
  ],
  imports: [
    CommonModule,
    NgbModalModule,
    ToIconTemplateModule,
    ToContentTemplateModule,
    HasHtmlContentModule,
    ToHtmlContentTextModule
  ],
  exports: [
    BsDialogComponent
  ]
})
export class BsDialogModule {

  public static forRoot(): ModuleWithProviders<BsDialogModule> {
    return {
      ngModule: BsDialogModule,
      providers: [
        {
          provide: DIALOG_SERVICE,
          useFactory: defaultBsDialogServiceFactory,
          deps: [Injector]
        }
      ]
    };
  }
}
